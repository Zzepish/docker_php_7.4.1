FROM php:7.4.1-fpm

RUN apt-get update
RUN apt-get install -y apt-transport-https \
                    ca-certificates \
                    curl \
                    wget \
                    libpcre3-dev \
                    libpq-dev \
                    libmemcached-dev \
                    libicu-dev \
                    zlib1g-dev \
                    libpng-dev \
                    libxslt-dev \
                    libzip-dev \
                    libjpeg-dev \
                    libfreetype6-dev \
                    libjpeg62-turbo-dev \
                    librabbitmq-dev

RUN pecl install xdebug-2.9.0 amqp

RUN docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/

RUN docker-php-ext-install opcache \
                           pdo_mysql \
                           pgsql \
                           pdo_pgsql \
                           intl \
                           gd \
                           bcmath \
                           xsl \
                           soap \
                           zip \
                           sockets
                           
RUN rm -rf /var/lib/apt/lists/* \
    && apt-get clean \
    && apt-get autoclean \
    && apt-get autoremove \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN chmod -R 0777 /var/log

STOPSIGNAL SIGQUIT

EXPOSE 9000
CMD /bin/bash